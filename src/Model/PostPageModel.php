<?php
declare(strict_types=1);

namespace App\Model;

class PostPageModel
{
    private int $page;

    private array $posts;

    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return PostModel[]
     */
    public function getPosts(): array
    {
        return $this->posts;
    }
}
