<?php
declare(strict_types=1);

namespace App\Model;

class PostUserReportModel
{
    private array $averageCharLength = [];
    private array $longestPost = [];
    private array $totalPosts = [];
    private array $averageUserCharLength = [];

    public function __construct($averageCharLength, $longestPost, $totalPosts, $averageUserCharLength)
    {
        $this->averageCharLength = $averageCharLength;
        $this->longestPost = $longestPost;
        $this->totalPosts = $totalPosts;
        $this->averageUserCharLength = $averageUserCharLength;
    }

    public function getAverageCharLength(): array
    {
        return $this->averageCharLength;
    }

    public function getLongestPost(): array
    {
        return $this->longestPost;
    }

    public function getTotalPosts(): array
    {
        return $this->totalPosts;
    }

    public function getAverageUserCharLength(): array
    {
        return $this->averageUserCharLength;
    }
}
