<?php
declare(strict_types=1);

namespace App\Model\Supermetrics;

use App\Model\PostPageModel;

class PostResponseModel
{
    private PostPageModel $data;

    public function getData(): PostPageModel
    {
        return $this->data;
    }
}
