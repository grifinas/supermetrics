<?php
declare(strict_types=1);

namespace App\Model\Supermetrics;

class RegisterResponseDataModel
{
    private string $slToken;

    public function getSlToken(): string
    {
        return $this->slToken;
    }
}
