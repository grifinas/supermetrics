<?php
declare(strict_types=1);

namespace App\Model\Supermetrics;

class RegisterResponseModel
{
    private RegisterResponseDataModel $data;

    public function getData(): RegisterResponseDataModel
    {
        return $this->data;
    }
}
