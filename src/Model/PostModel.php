<?php
declare(strict_types=1);

namespace App\Model;

use DateTime;

class PostModel
{
    private string $id;

    private string $fromName;

    private string $fromId;

    private string $message;

    private string $type;

    private DateTime $createdTime;

    public function getId(): string
    {
        return $this->id;
    }

    public function getFromName(): string
    {
        return $this->fromName;
    }

    public function getFromId(): string
    {
        return $this->fromId;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getCreatedTime(): DateTime
    {
        return $this->createdTime;
    }
}
