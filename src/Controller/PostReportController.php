<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\PostAnalytics;

class PostReportController
{
    private PostAnalytics $postAnalytics;

    public function __construct(PostAnalytics $postAnalytics)
    {
        $this->postAnalytics = $postAnalytics;
    }

    public function get()
    {
        return $this->postAnalytics->getReport();
    }
}
