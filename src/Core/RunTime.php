<?php
declare(strict_types=1);

namespace App\Core;

use App\Core\Model\ResponseModel;
use JMS\Serializer\SerializerInterface;
use RuntimeException;

class RunTime
{
    private AutoWire $autoWire;
    private Router $router;

    public function __construct()
    {
        $this->autoWire = new AutoWire();
        $this->router = new Router();
    }

    public function run(): ResponseModel
    {
        $route = $this->router->get($_SERVER['REQUEST_URI']);

        if (!$route) {
            return new ResponseModel(null, ResponseModel::HTTP_NOT_FOUND);
        }

        $controller = $this->autoWire->get($route->getControllerClass());

        return $this->runController($controller, $route->getControllerMethod());
    }

    private function runController(object $controller, string $method): ResponseModel
    {
        $response = $controller->{$method}();

        switch (true) {
            case $response instanceof ResponseModel:
                return $response;
            case $response === null:
                return new ResponseModel(null);
            case is_array($response):
                return new ResponseModel(json_encode($response));
            case is_string($response):
                return new ResponseModel($response);
            case is_object($response):
                $content = $this->autoWire->get(SerializerInterface::class)->serialize($response, 'json');
                return new ResponseModel($content);
            default:
                throw new RuntimeException('Controller is supposed to return a response');
        }
    }
}
