<?php
declare(strict_types=1);

namespace App\Core;

use App\Core\Exception\FailedAutoWireParameterException;
use Exception;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Phpfastcache\Helper\Psr16Adapter;
use ReflectionClass;

class AutoWire
{
    private array $loaded = [];

    public function __construct()
    {
        // TODO not how it should be done
        $this->loaded[Psr16Adapter::class] = new Psr16Adapter(!empty($_ENV['USE_CACHE']) ? 'Files' : 'Devnull');
        $this->loaded[SerializerInterface::class] = (new SerializerBuilder())
            ->addMetadataDir(__DIR__ . '/../../config/serializer')
            ->build()
        ;
    }

    public function get(string $classname): object
    {
        return $this->loaded[$classname] ?? $this->load($classname);
    }

    private function load(string $classname): object
    {
        $reflection = new ReflectionClass($classname);

        $constructor = $reflection->getConstructor();

        if (!$constructor) {
            // Currently doesnt work with interfaces, cause im lazy
            if ($reflection->isInterface()) {
                throw new Exception('Cant autowire interfaces');
            }

            $this->loaded[$classname] = $reflection->newInstance();

            return $this->loaded[$classname];
        }

        $parameters = $constructor->getParameters();

        $loadedParameters = [];
        foreach ($parameters as $parameter) {
            $classToLoad = $parameter->getClass();
            if ($classToLoad === null) {
                throw new FailedAutoWireParameterException($parameter);
            }

            $loadedParameters[] = $this->get($classToLoad->getName());
        }

        $this->loaded[$classname] = $reflection->newInstanceArgs($loadedParameters);

        return $this->loaded[$classname];
    }
}
