<?php
declare(strict_types=1);

namespace App\Core\Model;

class ResponseModel
{
    public const HTTP_OK = 200;
    public const HTTP_NOT_FOUND = 404;

    private ?string $content;
    private int $code;

    public function __construct(?string $content, int $code = self::HTTP_OK)
    {
        $this->content = $content;
        $this->code = $code;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function getCode(): int
    {
        return $this->code;
    }
}
