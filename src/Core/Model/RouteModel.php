<?php
declare(strict_types=1);

namespace App\Core\Model;

class RouteModel
{
    private string $controllerClass;

    private string $controllerMethod;

    public function __construct(string $controllerClass, string $controllerMethod)
    {
        $this->controllerClass = $controllerClass;
        $this->controllerMethod = $controllerMethod;
    }

    public function getControllerClass(): string
    {
        return $this->controllerClass;
    }

    public function getControllerMethod(): string
    {
        return $this->controllerMethod;
    }
}
