<?php
declare(strict_types=1);

namespace App\Core;

use App\Core\Model\RouteModel;

class Router
{
    private array $routes;

    public function __construct()
    {
        $this->routes = json_decode(file_get_contents(__DIR__ . '/../../config/routes.json'), true);
    }

    public function get(string $uri): ?RouteModel
    {
        $route = $this->routes[$uri] ?? [];

        return $route ? new RouteModel($route[0], $route[1]) : null;
    }
}
