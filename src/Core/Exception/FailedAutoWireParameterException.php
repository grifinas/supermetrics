<?php
declare(strict_types=1);

namespace App\Core\Exception;

use Exception;
use ReflectionParameter;
use Throwable;

class FailedAutoWireParameterException extends Exception
{
    public function __construct(ReflectionParameter $parameter, $code = 0, Throwable $previous = null)
    {
        $message = sprintf(
            'Cant autowire parameter %s, expected to be object, but %s found',
            $parameter->getName(),
            $parameter->getType()
        );

        parent::__construct($message, $code, $previous);
    }
}
