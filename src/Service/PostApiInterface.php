<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\PostModel;
use App\Model\PostPageModel;
use Generator;

interface PostApiInterface
{
    public function register(): void;

    public function posts(int $page = 1): PostPageModel;

    /**
     * @return PostModel[]|Generator
     */
    public function iterateAllPosts(): Generator;
}
