<?php
declare(strict_types=1);

namespace App\Service\Supermetrics;

use App\Model\Supermetrics\PostResponseModel;
use App\Model\Supermetrics\RegisterResponseModel;
use App\Service\GuzzleProvider;
use GuzzleHttp\Client;
use JMS\Serializer\SerializerInterface;

class ApiGateway
{
    private const BASE_URI = 'https://api.supermetrics.com';
    private const TIMEOUT = 30;
    private const CLIENT_ID = 'ju16a6m81mhid5ue1z3v2g0uh';
    private const EMAIL_ADDRESS = 'grifinas3@gmail.com';
    private const NAME = 'Domantas Bogdanas';

    private const ENDPOINT_REGISTER = '/assignment/register';
    private const ENDPOINT_POSTS = '/assignment/posts';

    private Client $client;
    private SerializerInterface $serializer;

    public function __construct(GuzzleProvider $guzzleProvider, SerializerInterface $serializer)
    {
        $this->client = $guzzleProvider->get([
            'base_uri' => self::BASE_URI,
            'timeout' => self::TIMEOUT
        ]);

        $this->serializer = $serializer;
    }

    public function register(): string
    {
        $response = $this->client->post(self::ENDPOINT_REGISTER, [
            'form_params' => [
                'client_id' => self::CLIENT_ID,
                'email' => self::EMAIL_ADDRESS,
                'name' => self::NAME
            ]
        ]);

        /** @var RegisterResponseModel $model */
        $model = $this->serializer->deserialize(
            $response->getBody()->getContents(),
            RegisterResponseModel::class,
            'json'
        );

        // TODO validation is kind of needed here, model doesn't necessarily adhere to the structure

        return $model
            ->getData()
            ->getSlToken()
        ;
    }

    public function posts(string $token, int $page = 1): PostResponseModel
    {
        $response = $this->client->get(self::ENDPOINT_POSTS, [
            'query' => [
                'sl_token' => $token,
                'page' => $page
            ]
        ]);

        return $this->serializer->deserialize(
            $response->getBody()->getContents(),
            PostResponseModel::class,
            'json'
        );
    }
}
