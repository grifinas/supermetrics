<?php
declare(strict_types=1);

namespace App\Service\Supermetrics;

use App\Model\PostPageModel;
use App\Service\PostApiInterface;
use DateTime;
use Generator;

class Api implements PostApiInterface
{
    private const TOKEN_EXPIRES_IN = 3600; // 1 hour
    private const MAX_ALLOWED_POST_PAGE_COUNT = 1000;

    private ApiGateway $apiGateway;
    private Cache $cache;
    private ?string $token = null;
    private ?DateTime $issuedAt = null;

    public function __construct(ApiGateway $apiGateway, Cache $cache)
    {
        $this->apiGateway = $apiGateway;
        $this->cache = $cache;

        $this->token = $cache->getToken();
        $this->issuedAt = $cache->getIssuedAt();
    }

    public function register(): void
    {
        $this->token = $this->apiGateway->register();
        $this->issuedAt = new DateTime();
        $this->cache
            ->setToken($this->token, self::TOKEN_EXPIRES_IN)
            ->setIssuedAt($this->issuedAt, self::TOKEN_EXPIRES_IN)
        ;
    }

    public function posts(int $page = 1): PostPageModel
    {
        $posts = $this->cache->getPosts($page);
        if ($posts) {
            return $posts;
        }

        if (!$this->isRegistered()) {
            $this->register();
        }

        $posts = $this->apiGateway
            ->posts($this->token, $page)
            ->getData()
        ;

        $this->cache->setPosts($page, $posts);

        return $posts;
    }

    public function iterateAllPosts(): Generator
    {
        $page = 1;
        $i = 0;
        while ($i++ < self::MAX_ALLOWED_POST_PAGE_COUNT) {
            $posts = $this->posts($page);

            // When a page larger that max pages is requested, last page is returned, use this as an exit strategy
            if ($posts->getPage() != $page) {
                break;
            }

            foreach ($posts->getPosts() as $post) {
                yield $post;
            }

            $page++;
        }
    }

    private function isRegistered(): bool
    {
        if (!$this->token || !$this->issuedAt) {
            return false;
        }

        $now = new DateTime();

        return ($now->getTimestamp() - $this->issuedAt->getTimestamp()) < self::TOKEN_EXPIRES_IN;
    }
}
