<?php
declare(strict_types=1);

namespace App\Service\Supermetrics;

use App\Model\PostPageModel;
use DateTime;
use Phpfastcache\Helper\Psr16Adapter;

class Cache
{
    private const KEY_TOKEN = 'Supermetrics.token';
    private const KEY_ISSUED_AT = 'Supermetrics.issued_at';
    private const KEY_POSTS = 'Supermetrics.posts.%s';
    private const TTL_POSTS = 3600; // 1 hour

    private Psr16Adapter $cache;

    public function __construct(Psr16Adapter $cache)
    {
        $this->cache = $cache;
    }

    public function setToken(string $token, int $ttl): self
    {
        $this->cache->set(self::KEY_TOKEN, $token, $ttl);

        return $this;
    }

    public function setIssuedAt(DateTime $issuedAt, int $ttl): self
    {
        $this->cache->set(self::KEY_ISSUED_AT, $issuedAt, $ttl);

        return $this;
    }

    public function setPosts(int $page, PostPageModel $posts): self
    {
        $this->cache->set(sprintf(self::KEY_POSTS, $page), $posts, self::TTL_POSTS);

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->cache->get(self::KEY_TOKEN, null);
    }

    public function getIssuedAt(): ?DateTime
    {
        return $this->cache->get(self::KEY_ISSUED_AT, null);
    }

    public function getPosts(int $page): ?PostPageModel
    {
        return $this->cache->get(sprintf(self::KEY_POSTS, $page), null);
    }
}
