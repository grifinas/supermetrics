<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\PostModel;
use App\Model\PostUserReportModel;
use App\Service\Supermetrics\Api as SupermetricsApi;
use DateTime;

class PostAnalytics
{
    private const MONTHS = 6;

    private PostApiInterface $postApi;

    public function __construct(SupermetricsApi $postApi)
    {
        $this->postApi = $postApi;
    }

    public function getReport(): PostUserReportModel
    {
        /** @var PostModel[] $longestPost */
        $longestPost = [];
        $characterLength = [];
        $postsByMonth = [];
        $postsByWeek = [];
        $allowedMonths = [];

        $i = 0;
        $date = new DateTime();
        while ($i++ <= self::MONTHS) {
            $month = $date->format('Y-m');
            $longestPost[$month] = null;
            $characterLength[$month] = [];
            $postsByMonth[$month] = [];
            $allowedMonths[$month] = true;
            $date->modify('-1 month');
        }

        foreach ($this->postApi->iterateAllPosts() as $postModel) {
            $month = $postModel->getCreatedTime()->format('Y-m');
            $week = $postModel->getCreatedTime()->format('W');

            // Skip posts older than selected range
            if (!isset($allowedMonths[$month])) {
                continue;
            }

            if (!isset($longestPost[$month])
                || mb_strlen($longestPost[$month]->getMessage()) < mb_strlen($postModel->getMessage())
            ) {
                $longestPost[$month] = $postModel;
            }

            $characterLength[$month][$postModel->getFromId()][] = mb_strlen($postModel->getMessage());
            $postsByMonth[$month][$postModel->getFromId()] = ($postsByMonth[$month][$postModel->getFromId()] ?? 0) + 1;
            $postsByWeek[$week] = ($postsByWeek[$week] ?? 0) + 1;
        }

        return $this->makeReport($characterLength, $longestPost, $postsByWeek, $postsByMonth);
    }

    private function makeReport($characterLength, $longestPost, $postsByWeek, $postsByMonth): PostUserReportModel
    {
        $averageCharLength = [];
        $averageUserCharLength = [];
        foreach ($characterLength as $month => $posts) {
            $totalCharacterLength = 0;
            foreach ($posts as $userPosts) {
                $totalCharacterLength += array_sum($userPosts);
            }

            $averageCharLength[$month] = !empty($postsByMonth[$month])
                ? $totalCharacterLength / array_sum($postsByMonth[$month])
                : 0
            ;
            $averageUserCharLength[$month] = [];
            foreach ($postsByMonth[$month] as $user => $count) {
                $averageUserCharLength[$month][$user] = $count > 0
                    ? array_sum($posts[$user]) / $count
                    : 0
                ;
            }
        }

        return new PostUserReportModel($averageCharLength, $longestPost, $postsByWeek, $averageUserCharLength);
    }
}
