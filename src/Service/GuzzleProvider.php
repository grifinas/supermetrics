<?php
declare(strict_types=1);

namespace App\Service;

use GuzzleHttp\Client;

/**
 * This class exist simply to invert dependencies and make testing easy
 */
class GuzzleProvider
{
    public function get(array $config = []): Client
    {
        return new Client($config);
    }
}
