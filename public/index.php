<?php
declare(strict_types=1);

use App\Core\RunTime;

require_once "../vendor/autoload.php";

$response = (new RunTime())->run();

http_response_code($response->getCode());
echo $response->getContent();
