## Setup

```
cd docker
docker-compose up --build
```

## Run tests
Exec into the container and run
```
vendor/bin/codecept run unit
```

## Access the endpoint
Endpoint with the transformed data should be visible here: http://localhost:8081/post/report

## Developer notes

Due to time constraints the following were skipped/cut-down:
* Core tests
* Autowiring service is pretty bare bones, has some hacks in it
* Api response validator (currently if the api changes the response code will fail ungracefully)
* All functional tests

Request to the api are currently cached. If that's not how it should work, there's an .env variable `USE_CACHE` that can be turned off (will require you to run `docker-compose up` again)


