<?php
declare(strict_types=1);

namespace unit\Service;

use App\Model\PostModel;
use App\Service\PostAnalytics;
use App\Service\Supermetrics\Api as SupermetricsApi;
use Codeception\Test\Unit;
use DateTime;
use PHPUnit\Framework\MockObject\MockObject;

class PostAnalyticsTest extends Unit
{
    private PostAnalytics $analytics;

    /**
     * @var MockObject|SupermetricsApi
     */
    private SupermetricsApi $postApi;

    protected function _before()
    {
        $this->postApi = $this->createMock(SupermetricsApi::class);

        $this->analytics = new PostAnalytics($this->postApi);
    }

    public function testCorrectStructureWhenNoPostsExist(): void
    {
        $this->postApi
            ->expects(self::once())
            ->method('iterateAllPosts')
            ->willReturnCallback(function () {
                foreach ([] as $e) {
                    yield $e;
                }
            })
        ;

        $report = $this->analytics->getReport();

        $averageCharLength = $report->getAverageCharLength();
        $longestPost = $report->getLongestPost();
        $averageUserCharLength = $report->getAverageUserCharLength();
        foreach ($this->getExpectedMonths() as $month) {
            self::assertArrayHasKey($month, $averageCharLength);
            self::assertArrayHasKey($month, $longestPost);
            self::assertArrayHasKey($month, $averageUserCharLength);
            self::assertEquals(0, $averageCharLength[$month]);
            self::assertNull($longestPost[$month]);
            self::assertEmpty($averageUserCharLength[$month]);
        }
        self::assertEmpty($report->getTotalPosts());
    }

    public function testCorrectReportIsGenerated(): void
    {
        $this->postApi
            ->expects(self::once())
            ->method('iterateAllPosts')
            ->willReturnCallback(function () {
                foreach ($this->getPosts() as $e) {
                    yield $e;
                }
            })
        ;

        $report = $this->analytics->getReport();

        $averageCharLength = $report->getAverageCharLength();
        $longestPost = $report->getLongestPost();
        $averageUserCharLength = $report->getAverageUserCharLength();
        $totalByWeek = $report->getTotalPosts();

        $month = (new DateTime())->format('Y-m');
        $week = (new DateTime())->format('W');
        self::assertAround(2, $averageCharLength[$month]);
        self::assertEquals('user2', $longestPost[$month]->getFromId());
        self::assertAround(1.5, $averageUserCharLength[$month]['user1']);
        self::assertEquals(3, $averageUserCharLength[$month]['user2']);
        self::assertEquals(3, $totalByWeek[$week]);

        $month = (new DateTime('-1 month'))->format('Y-m');
        $week = (new DateTime('-1 month'))->format('W');
        self::assertAround(3, $averageCharLength[$month]);
        self::assertEquals('user2', $longestPost[$month]->getFromId());
        self::assertAround(2.5, $averageUserCharLength[$month]['user1']);
        self::assertEquals(4, $averageUserCharLength[$month]['user2']);
        self::assertEquals(3, $totalByWeek[$week]);

        $month = (new DateTime('-3 months'))->format('Y-m');
        $week = (new DateTime('-3 months'))->format('W');
        self::assertAround(5, $averageCharLength[$month]);
        self::assertEquals('user1', $longestPost[$month]->getFromId());
        self::assertEquals(5, $averageUserCharLength[$month]['user1']);
        self::assertEquals(5, $averageUserCharLength[$month]['user2']);
        self::assertEquals(2, $totalByWeek[$week]);

        $month = (new DateTime('-10 months'))->format('Y-m');
        $week = (new DateTime('-10 months'))->format('W');
        self::assertArrayNotHasKey($month, $averageCharLength);
        self::assertArrayNotHasKey($month, $longestPost);
        self::assertArrayNotHasKey($month, $averageUserCharLength);
        self::assertArrayNotHasKey($week, $totalByWeek);
    }

    private function getPosts(): array
    {
        return [
            $this->createPost('user1', 1, new DateTime()),
            $this->createPost('user2', 3, new DateTime()),
            $this->createPost('user1', 2, new DateTime()),

            $this->createPost('user1', 2, new DateTime('-1 month')),
            $this->createPost('user2', 4, new DateTime('-1 month')),
            $this->createPost('user1', 3, new DateTime('-1 month')),

            $this->createPost('user1', 5, new DateTime('-3 months')),
            $this->createPost('user2', 5, new DateTime('-3 months')),

            $this->createPost('user1', 5, new DateTime('-10 months')),
            $this->createPost('user2', 5, new DateTime('-10 months')),
        ];
    }

    private function createPost(string $fromId, int $message, DateTime $createdAt): PostModel
    {
        $post = $this->createMock(PostModel::class);
        $post
            ->expects(self::any())
            ->method('getCreatedTime')
            ->willReturn($createdAt)
        ;

        $post
            ->expects(self::any())
            ->method('getMessage')
            ->willReturnCallback(fn() => str_repeat('a', $message))
        ;

        $post
            ->expects(self::any())
            ->method('getFromId')
            ->willReturn($fromId)
        ;

        return $post;
    }

    private function getExpectedMonths(): array
    {
        $i = 0;
        $date = new DateTime();
        $months = [];
        while ($i++ <= 6) {
            $months[] = $date->format('Y-m');
            $date->modify('-1 month');
        }

        return $months;
    }

    private function assertAround($expected, $actual, $precision = 0.1)
    {
        if ($precision < abs($expected - $actual)) {
            self::fail("Expected that value $actual would be around $expected (rounded to $precision), but it was not");
        }
    }
}
