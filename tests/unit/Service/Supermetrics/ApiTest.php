<?php
declare(strict_types=1);

namespace unit\Service\Supermetrics;

use App\Model\PostPageModel;
use App\Model\Supermetrics\PostResponseModel;
use App\Service\Supermetrics\Api;
use App\Service\Supermetrics\ApiGateway;
use App\Service\Supermetrics\Cache;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;

class ApiTest extends Unit
{
    private const TOKEN_EXPIRES_IN = 3600; // 1 hour
    private const MAX_ALLOWED_POST_PAGE_COUNT = 1000;

    private Api $api;

    /**
     * @var ApiGateway|MockObject
     */
    private ApiGateway $gateway;
    /**
     * @var Cache|MockObject
     */
    private Cache $cache;

    protected function _before()
    {
        $this->gateway = $this->createMock(ApiGateway::class);
        $this->cache = $this->createMock(Cache::class);

        $this->cache
            ->expects(self::once())
            ->method('getToken')
            ->willReturn(null)
        ;

        $this->cache
            ->expects(self::once())
            ->method('getIssuedAt')
            ->willReturn(null)
        ;

        $this->api = new Api($this->gateway, $this->cache);
    }

    public function testRegister(): void
    {
        $this->setUpRegistration();

        $this->api->register();
    }

    public function testPostsFromCache(): void
    {
        $page = 7;

        $this->cache
            ->expects(self::once())
            ->method('getPosts')
            ->with($page)
            ->willReturn(new PostPageModel())
        ;

        $this->gateway
            ->expects(self::never())
            ->method('posts')
        ;

        $this->api->posts($page);
    }

    public function testPostsFromApi(): void
    {
        $page = 7;

        $this->cache
            ->expects(self::once())
            ->method('getPosts')
            ->with($page)
            ->willReturn(null)
        ;

        $this->setUpRegistration();

        $postPage = new PostPageModel();
        $response = $this->createMock(PostResponseModel::class);
        $response
            ->expects(self::once())
            ->method('getData')
            ->willReturn($postPage)
        ;

        $this->gateway
            ->expects(self::once())
            ->method('posts')
            ->with('token', $page)
            ->willReturn($response)
        ;

        $this->cache
            ->expects(self::once())
            ->method('setPosts')
            ->with($page, $postPage)
            ->willReturnSelf()
        ;

        $realResponse = $this->api->posts($page);

        self::assertEquals($postPage, $realResponse);
    }

    public function testIterateAllPostsHasInfinitePages(): void
    {
        $this->cache
            ->expects(self::exactly(self::MAX_ALLOWED_POST_PAGE_COUNT))
            ->method('getPosts')
            ->willReturnCallback(function ($page) {
                $postPage = $this->createMock(PostPageModel::class);
                $postPage
                    ->expects(self::once())
                    ->method('getPage')
                    ->willReturn($page)
                ;

                $postPage
                    ->expects(self::once())
                    ->method('getPosts')
                    ->willReturn([])
                ;

                return $postPage;
            })
        ;

        foreach ($this->api->iterateAllPosts() as $post) {}
    }

    public function testIterateAllPostsBreaksWhenNoNewPagesExist(): void
    {
        $this->cache
            ->expects(self::exactly(4))
            ->method('getPosts')
            ->willReturnCallback(function ($page) {
                $postPage = $this->createMock(PostPageModel::class);
                $postPage
                    ->expects(self::once())
                    ->method('getPage')
                    ->willReturn(min($page, 3))
                ;

                $postPage
                    ->expects(self::atMost(1))
                    ->method('getPosts')
                    ->willReturn([])
                ;

                return $postPage;
            })
        ;

        foreach ($this->api->iterateAllPosts() as $post) {}
    }

    private function setUpRegistration(string $token = 'token'): void
    {
        $this->gateway
            ->expects(self::once())
            ->method('register')
            ->willReturn($token)
        ;

        $this->cache
            ->expects(self::once())
            ->method('setToken')
            ->with($token, self::TOKEN_EXPIRES_IN)
            ->willReturnSelf()
        ;

        $this->cache
            ->expects(self::once())
            ->method('setIssuedAt')
            ->willReturnSelf()
        ;
    }
}
