<?php
declare(strict_types=1);

namespace unit\Service\Supermetrics;

use App\Model\Supermetrics\PostResponseModel;
use App\Model\Supermetrics\RegisterResponseDataModel;
use App\Model\Supermetrics\RegisterResponseModel;
use App\Service\GuzzleProvider;
use App\Service\Supermetrics\ApiGateway;
use Codeception\Test\Unit;
use GuzzleHttp\Client;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class ApiGatewayTest extends Unit
{
    private const BASE_URI = 'https://api.supermetrics.com';
    private const TIMEOUT = 30;
    private const CLIENT_ID = 'ju16a6m81mhid5ue1z3v2g0uh';
    private const EMAIL_ADDRESS = 'grifinas3@gmail.com';
    private const NAME = 'Domantas Bogdanas';

    private const ENDPOINT_REGISTER = '/assignment/register';
    private const ENDPOINT_POSTS = '/assignment/posts';


    private ApiGateway $gateway;

    /**
     * @var Client|MockObject
     */
    private Client $client;

    /**
     * @var SerializerInterface|MockObject
     */
    private SerializerInterface $serializer;

    protected function _before()
    {
        $this->client = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->addMethods(['get', 'post'])
            ->getMock()
        ;

        $guzzleProvider = $this->createMock(GuzzleProvider::class);
        $guzzleProvider
            ->expects(self::once())
            ->method('get')
            ->with([
                'base_uri' => self::BASE_URI,
                'timeout' => self::TIMEOUT
            ])
            ->willReturn($this->client)
        ;
        $this->serializer = $this->createMock(SerializerInterface::class);

        $this->gateway = new ApiGateway($guzzleProvider, $this->serializer);
    }

    public function testRegister(): void
    {
        $response = 'response';
        $token = 'token';

        $this->client
            ->expects(self::once())
            ->method('post')
            ->with(self::ENDPOINT_REGISTER, [
                'form_params' => [
                    'client_id' => self::CLIENT_ID,
                    'email' => self::EMAIL_ADDRESS,
                    'name' => self::NAME
                ]
            ])
            ->willReturn($this->mockResponse($response))
        ;

        $data = $this->createMock(RegisterResponseDataModel::class);
        $data
            ->expects(self::once())
            ->method('getSlToken')
            ->willReturn($token)
        ;

        $responseModel = $this->createMock(RegisterResponseModel::class);
        $responseModel
            ->expects(self::once())
            ->method('getData')
            ->willReturn($data)
        ;

        $this->serializer
            ->expects(self::once())
            ->method('deserialize')
            ->with($response, RegisterResponseModel::class, 'json')
            ->willReturn($responseModel)
        ;

        self::assertEquals($token, $this->gateway->register());
    }

    public function testPosts(): void
    {
        $response = 'response';
        $token = 'token';
        $page = 1;

        $this->client
            ->expects(self::once())
            ->method('get')
            ->with(self::ENDPOINT_POSTS, [
                'query' => [
                    'sl_token' => $token,
                    'page' => $page
                ]
            ])
            ->willReturn($this->mockResponse($response))
        ;

        $responseModel = new PostResponseModel();
        $this->serializer
            ->expects(self::once())
            ->method('deserialize')
            ->with($response, PostResponseModel::class, 'json')
            ->willReturn($responseModel)
        ;

        self::assertEquals(
            $responseModel,
            $this->gateway->posts($token, $page)
        );
    }

    private function mockResponse($contents = ''): ResponseInterface
    {
        $response = $this->createMock(ResponseInterface::class);

        $body = $this->createMock(StreamInterface::class);

        $body
            ->expects(self::any())
            ->method('getContents')
            ->willReturn($contents)
        ;

        $response
            ->expects(self::any())
            ->method('getBody')
            ->willReturn($body)
        ;

        return $response;
    }
}
