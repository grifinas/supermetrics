<?php
declare(strict_types=1);

namespace unit\Service\Supermetrics;

use App\Model\PostPageModel;
use App\Model\PostResponseModel;
use App\Service\Supermetrics\Cache;
use Codeception\Test\Unit;
use DateTime;
use Phpfastcache\Helper\Psr16Adapter;
use PHPUnit\Framework\MockObject\MockObject;

class CacheTest extends Unit
{
    private const KEY_TOKEN = 'Supermetrics.token';
    private const KEY_ISSUED_AT = 'Supermetrics.issued_at';
    private const KEY_POSTS = 'Supermetrics.posts.%s';
    private const TTL_POSTS = 3600; // 1 hour

    private Cache $cache;

    /**
     * @var Psr16Adapter|MockObject
     */
    private Psr16Adapter $adapter;

    protected function _before()
    {
        $this->adapter = $this->createMock(Psr16Adapter::class);

        $this->cache = new Cache($this->adapter);
    }

    public function testTokenIsCachedCorrectly(): void
    {
        $ttl = 3600;
        $tokenValue = 'token';
        
        $this->adapter
            ->expects(self::once())
            ->method('set')
            ->with(self::KEY_TOKEN, $tokenValue, $ttl)
        ;
        
        $this->adapter
            ->expects(self::once())
            ->method('get')
            ->with(self::KEY_TOKEN, null)
            ->willReturn($tokenValue)
        ;
        
        $this->cache->setToken($tokenValue, $ttl);
        self::assertEquals($tokenValue, $this->cache->getToken());
    }

    public function testIssuedAtIsCachedCorrectly()
    {
        $ttl = 3600;
        $issuedAt = new DateTime();

        $this->adapter
            ->expects(self::once())
            ->method('set')
            ->with(self::KEY_ISSUED_AT, $issuedAt, $ttl)
        ;

        $this->adapter
            ->expects(self::once())
            ->method('get')
            ->with(self::KEY_ISSUED_AT, null)
            ->willReturn($issuedAt)
        ;

        $this->cache->setIssuedAt($issuedAt, $ttl);
        self::assertEquals($issuedAt, $this->cache->getIssuedAt());
    }

    public function testPostsAreCachedCorrectly(): void
    {
        $page = 1;
        $posts = new PostPageModel();

        $this->adapter
            ->expects(self::once())
            ->method('set')
            ->with(sprintf(self::KEY_POSTS, $page), $posts, self::TTL_POSTS)
        ;

        $this->adapter
            ->expects(self::once())
            ->method('get')
            ->with(sprintf(self::KEY_POSTS, $page), null)
            ->willReturn($posts)
        ;

        $this->cache->setPosts($page, $posts);
        self::assertEquals($posts, $this->cache->getPosts($page));
    }
}
