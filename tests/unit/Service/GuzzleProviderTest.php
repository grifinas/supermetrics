<?php
declare(strict_types=1);

namespace unit\Service;

use App\Service\GuzzleProvider;
use Codeception\Test\Unit;

class GuzzleProviderTest extends Unit
{
    public function testGetCreateClient(): void
    {
        $provider = new GuzzleProvider();
        $config = [
            'base_uri' => 'foo',
            'timeout' => 1
        ];

        $client = $provider->get($config);

        self::assertEquals($config['base_uri'], (string)$client->getConfig()['base_uri']);
        self::assertEquals($config['timeout'], $client->getConfig()['timeout']);
    }
}
